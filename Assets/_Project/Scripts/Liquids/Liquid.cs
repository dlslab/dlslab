﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Liquid : MonoBehaviour
{
    [SerializeField] private int _code;
    [SerializeField] private string _name;
    [SerializeField] private Color _color;

    public string Name => _name;
    public int Code => _code;
    public Color Color => _color;

    public Liquid(int code, Color color)
    {
        this._code = code;
        this._color = color;
    }

    public override int GetHashCode()
    {
        return Code;
    }

    public override bool Equals(object obj)
    {
        return obj is Liquid && ((Liquid) obj).Code == Code;
    }
}