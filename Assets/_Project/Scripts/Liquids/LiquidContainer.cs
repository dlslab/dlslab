using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiquidContainer : MonoBehaviour
{

    [SerializeField] private float _maxCapacity = 0f;
    [SerializeField] private float _currentCapacity = 0f;
    [SerializeField] private Liquid _liquid;

    private MeshRenderer meshRenderer;

    public float PartOfTotal => _currentCapacity / _maxCapacity;

    public Color Color => _liquid.Color;

    public void Add(Liquid liquid, float quantity)
    {
        if(liquid != this._liquid)
        {
            this._liquid = new Liquid(-1, Color.gray);
        }

        _currentCapacity += quantity;
        _currentCapacity = Mathf.Clamp(_currentCapacity, 0, _maxCapacity);
    }

    private void UpdateVisual()
    {
        //Debug.Log(PartOfTotal);
        meshRenderer.material.SetFloat("_LiquidHeight", PartOfTotal);
        meshRenderer.material.SetColor("_LiquidColor", Color);
    }

    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateVisual();
    }
}
