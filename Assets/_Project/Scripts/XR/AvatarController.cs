using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;
using Unity.Netcode;
using Unity.Collections;

public class AvatarController : NetworkBehaviour
{

    private NetworkVariable<Vector3> rigPosition = new NetworkVariable<Vector3>();
    private NetworkVariable<Quaternion> rigRotation = new NetworkVariable<Quaternion>();
    private GameObject rigReference;

    private NetworkVariable<Vector3> rightHandPosition = new NetworkVariable<Vector3>();
    private NetworkVariable<Quaternion> rightHandAngle = new NetworkVariable<Quaternion>();
    private NetworkVariable<bool> rightHandGripping = new NetworkVariable<bool>();
    private GameObject rightHandReference;
    [SerializeField] private GameObject rightHandObject;
    [SerializeField] private InputActionReference rightGripAction;

    private NetworkVariable<Vector3> leftHandPosition = new NetworkVariable<Vector3>();
    private NetworkVariable<Quaternion> leftHandAngle = new NetworkVariable<Quaternion>();
    private NetworkVariable<bool> leftHandGripping = new NetworkVariable<bool>();
    private GameObject leftHandReference;
    [SerializeField] private GameObject leftHandObject;
    [SerializeField] private InputActionReference leftGripAction;

    private NetworkVariable<Vector3> headPosition = new NetworkVariable<Vector3>();
    private NetworkVariable<Quaternion> headRotation = new NetworkVariable<Quaternion>();
    private GameObject headReference;
    [SerializeField] private GameObject headObject;

    [SerializeField] private GameObject bodyObject;

    [SerializeField] private float nameTagHeight = 0.5f;
    [SerializeField] private GameObject nameTagObject;
    private NetworkVariable<FixedString64Bytes> _playerName = new NetworkVariable<FixedString64Bytes>("TEST");

    //public string TestPlayerName = "TEST";

    public string PlayerName
    {
        get { return _playerName.Value.ToString(); }
        set { UpdatePlayerNameServerRPC(value); }
    }

    /*public Vector3 rightHandPosition = new Vector3();
    public Quaternion rightHandAngle = new Quaternion();
    public bool rightHandGripping = false;
    [SerializeField] private GameObject rightHandReference;
    [SerializeField] private GameObject rightHandObject;
    [SerializeField] private InputActionReference rightGripAction;

    public Vector3 leftHandPosition = new Vector3();
    public Quaternion leftHandAngle = new Quaternion();
    public bool leftHandGripping = false;
    [SerializeField] private GameObject leftHandReference;
    [SerializeField] private GameObject leftHandObject;
    [SerializeField] private InputActionReference leftGripAction;*/

    // Start is called before the first frame update
    void Start()
    {
        rigReference = GameObject.Find("XR Origin");
        rightHandReference = GameObject.Find("RightHand Controller");
        leftHandReference = GameObject.Find("LeftHand Controller");
        headReference = GameObject.Find("Main Camera");

        if (IsOwner)
        {
           headObject.SetActive(false);
           bodyObject.SetActive(false);
           nameTagObject.SetActive(false);

           UpdatePlayerNameServerRPC(UserInfos.Instance.UserName);
        }
        else
            nameTagObject.GetComponent<TextMesh>().text = PlayerName;
    }

    public void MoveObject(GameObject obj, Vector3 position, Quaternion rotation)
    {
        obj.transform.position = position;
        obj.transform.rotation = rotation;
    }

    public void SetGripping(GameObject handObject, bool isGripping)
    {
        HandGestures handGestures = handObject.GetComponent<HandGestures>();
        handGestures.isGripping = isGripping;
    }

    public void PlaceBody()
    {
        Vector3 headPosition = headObject.transform.position;
        float bodySize = bodyObject.GetComponent<Renderer>().bounds.size.y;

        float bodyHeight = headPosition.y - (bodySize / 2);
        bodyObject.transform.position = new Vector3(headPosition.x, bodyHeight, headPosition.z);

        nameTagObject.transform.position = new Vector3(headPosition.x, headPosition.y + nameTagHeight, headPosition.z);
        nameTagObject.transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
        nameTagObject.GetComponent<TextMesh>().text = PlayerName;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(PlayerName);

        if (IsOwner)
        {
            Transform transform = rigReference.transform;
            SetRigServerRpc(transform.position, transform.rotation);
            MoveObject(this.gameObject, transform.position, transform.rotation);

            transform = headReference.transform;
            SetHeadServerRpc(transform.position, transform.rotation);

            transform = rightHandReference.transform;
            bool isGripping = rightGripAction.action.ReadValue<float>() == 1;
            SetRightHandServerRpc(transform.position, transform.rotation, isGripping);
            MoveObject(rightHandObject, transform.position, transform.rotation);
            SetGripping(rightHandObject, isGripping);

            transform = leftHandReference.transform;
            isGripping = leftGripAction.action.ReadValue<float>() == 1;
            SetLeftHandServerRpc(transform.position, transform.rotation, isGripping);
            MoveObject(leftHandObject, transform.position, transform.rotation);
            SetGripping(leftHandObject, isGripping);
        }
        else
        {
            MoveObject(this.gameObject, rigPosition.Value, rigRotation.Value);

            MoveObject(headObject, headPosition.Value, headRotation.Value);

            MoveObject(leftHandObject, leftHandPosition.Value, leftHandAngle.Value);
            SetGripping(leftHandObject, leftHandGripping.Value);

            MoveObject(rightHandObject, rightHandPosition.Value, rightHandAngle.Value);
            SetGripping(rightHandObject, rightHandGripping.Value);

            PlaceBody();
        }

        /*Transform transformRight = rightHandReference.transform;
        rightHandPosition = transformRight.position;
        rightHandAngle = transformRight.rotation;
        rightHandGripping = rightGripAction.action.ReadValue<float>() == 1;



        Transform transformLeft = leftHandReference.transform;
        leftHandPosition = transformLeft.position;
        leftHandAngle = transformLeft.rotation;
        leftHandGripping = leftGripAction.action.ReadValue<float>() == 1;

        MoveHand(rightHandObject, rightHandPosition, rightHandAngle);
        SetGripping(rightHandObject, rightHandGripping);

        MoveHand(leftHandObject, leftHandPosition, leftHandAngle);
        SetGripping(leftHandObject, leftHandGripping);*/


    }

    [ServerRpc]
    public void SetRightHandServerRpc(Vector3 handPosition, Quaternion handRotation, bool handGripping)
    {
        //Debug.Log("Server RPC RIGHT");

        rightHandPosition.Value = handPosition;
        rightHandAngle.Value = handRotation;
        rightHandGripping.Value = handGripping;
    }

    [ServerRpc]
    public void SetLeftHandServerRpc(Vector3 handPosition, Quaternion handRotation, bool handGripping)
    {
        leftHandPosition.Value = handPosition;
        leftHandAngle.Value = handRotation;
        leftHandGripping.Value = handGripping;
    }

    [ServerRpc]
    public void SetHeadServerRpc(Vector3 headPosition, Quaternion headRotation)
    {
        this.headPosition.Value = headPosition;
        this.headRotation.Value = headRotation;
    }

    [ServerRpc]
    public void SetRigServerRpc(Vector3 rigPosition, Quaternion rigRotation)
    {
        this.rigPosition.Value = rigPosition;
        this.rigRotation.Value = rigRotation;
    }

    [ServerRpc]
    private void UpdatePlayerNameServerRPC(string name)
    {
        _playerName.Value = new FixedString64Bytes(name);
    }
}
