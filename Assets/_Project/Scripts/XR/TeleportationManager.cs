using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class TeleportationManager : MonoBehaviour
{

    [SerializeField] private InputActionAsset actionAsset;
    [SerializeField] private XRRayInteractor teleportRay;
    [SerializeField] private TeleportationProvider teleportProvider;
    
    private InputAction moveStick;

    private bool activated;
    
    // Start is called before the first frame update
    void Start()
    {
        InputActionMap actionMap = actionAsset.FindActionMap("XRI LeftHand");
        
        InputAction activateTp = actionMap.FindAction("Teleport Mode Activate");
        InputAction cancelTp = actionMap.FindAction(("Teleport Mode Cancel"));
        moveStick = actionMap.FindAction("Move");
        
        activateTp.Enable();
        cancelTp.Enable();
        moveStick.Enable();

        activateTp.performed += onTeleportActivate;
        cancelTp.performed += onTeleportCancel;

        teleportRay.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (activated && !moveStick.triggered)
        {
            RaycastHit hitPosition;
            bool findPosition = teleportRay.TryGetCurrent3DRaycastHit(out hitPosition);

            TeleportRequest req = new TeleportRequest()
            {
                destinationPosition =  hitPosition.point
            };

            teleportProvider.QueueTeleportRequest(req);
            teleportRay.enabled = false;
            activated = false;
        }
    }

    private void onTeleportActivate(InputAction.CallbackContext teleportContext)
    {
        teleportRay.enabled = true;
        activated = true;
    }

    private void onTeleportCancel(InputAction.CallbackContext teleportContext)
    {
        teleportRay.enabled = false;
        activated = false;
    }
}
