using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HandGestures : MonoBehaviour
{

    [SerializeField] private bool animateHand = true;
    [SerializeField] private float animationSpeed = 0.05f;
    [SerializeField] public bool isGripping = false;
    
    private Animator animator; 
    private float grip = 0.0f;

    //private NetworkVariable<bool> isGripping = new NetworkVariable<bool>();

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isGripping && grip <= 1.0)
        {
            grip += animationSpeed;
        }
        else if(!isGripping && grip > 0)
        {
            grip -= animationSpeed;
        }

        AnimateGrip();
    }

    private void AnimateGrip()
    {
        if (animateHand)
        {
            animator.SetFloat("Thumb", grip);
            animator.SetFloat("Index", grip);
            animator.SetFloat("Middle", grip);
            animator.SetFloat("Ring", grip);
            animator.SetFloat("Pinky", grip);
        }
    }

    private void AnimatePointing()
    {
        animator.SetFloat("Thumb", 1.0f);
        animator.SetFloat("Index", 0.0f);
        animator.SetFloat("Middle", 1.0f);
        animator.SetFloat("Ring", 1.0f);
        animator.SetFloat("Pinky", 1.0f);
    }
    
    /*private void FindDevice()
    {
        InputDeviceCharacteristics deviceCharacteristics =
            (hand == HandType.Left) ? InputDeviceCharacteristics.Left : InputDeviceCharacteristics.Right;
        List<InputDevice> validDevices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(deviceCharacteristics, validDevices);

        if (validDevices.Count > 0)
            device = validDevices[0];
    }*/
}
