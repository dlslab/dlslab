using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using Unity.XR.CoreUtils;
using UnityEngine.XR.Interaction.Toolkit;

public class DeviceAdapter : MonoBehaviour
{
    private InputDevice xrDevice;
    [SerializeField]
    private XROrigin xrRig;

    [SerializeField]
    private float resolutionScale = 1f;
    // Start is called before the first frame update
    void Start()
    {
        /*if (xrRig == null)
        {
            xrRig = GameObject.Find("XR Rig").GetComponent<XRRig>();
        }
        
        var devices = new List<InputDevice>();
        InputDevices.GetDevices(devices);

        foreach (var d in devices)
            Debug.Log(d);

        xrDevice = InputDevices.GetDeviceAtXRNode(XRNode.Head);
        Debug.Log(xrDevice.name);

        if(isOculus(xrDevice))
        {
            xrRig.requestedTrackingOriginMode = XRRig.TrackingOriginMode.Device;
            xrRig.cameraYOffset = 1.5f;
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        XRSettings.eyeTextureResolutionScale = resolutionScale;
    }

    private bool isOculus(InputDevice inputDevice)
    {
        return inputDevice.name.ToLower().Contains("oculus");
    }
}
