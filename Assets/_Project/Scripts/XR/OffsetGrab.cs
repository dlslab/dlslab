using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using Unity.Netcode;

/// <summary>
/// Small modification of the classic XRGrabInteractable that will keep the position and rotation offset between the
/// grabbed object and the controller instead of snapping the object to the controller. Better for UX and the illusion
/// of holding the thing (see Tomato Presence : https://owlchemylabs.com/tomatopresence/)
/// </summary>
public class OffsetGrab : XRGrabInteractable
{
    class SavedTransform
    {
        public Vector3 OriginalPosition;
        public Quaternion OriginalRotation;
    }


    private Dictionary<XRBaseInteractor, SavedTransform> m_SavedTransforms = new Dictionary<XRBaseInteractor, SavedTransform>();
    private Rigidbody _rb;

    /// <summary>
    ///     May be null, if object is not OwnerAuthoritative
    /// </summary>
    private OwnerAuthoritativeTransform _networkGrab;
    

    protected override void Awake()
    {
        base.Awake();

        _rb = GetComponent<Rigidbody>();
        _networkGrab = GetComponent<OwnerAuthoritativeTransform>();
        
    }

    protected override void OnSelectEntering(SelectEnterEventArgs args)
    {
        XRBaseInteractor interactor = args.interactor;
        if (interactor is XRDirectInteractor)
        {
            SavedTransform savedTransform = new SavedTransform();

            savedTransform.OriginalPosition = interactor.attachTransform.localPosition;
            savedTransform.OriginalRotation = interactor.attachTransform.localRotation;

            m_SavedTransforms[interactor] = savedTransform;

            bool haveAttach = attachTransform != null;

            interactor.attachTransform.position = haveAttach ? attachTransform.position : _rb.worldCenterOfMass;
            interactor.attachTransform.rotation = haveAttach ? attachTransform.rotation : _rb.rotation;
        }

        if (_networkGrab != null)
        {
            _networkGrab.GetOwnership();//GetOwnershipServerRPC(NetworkManager.Singleton.LocalClientId, networkObject);
        }
            

        base.OnSelectEntering(args);
    }

    protected override void OnSelectExiting(SelectExitEventArgs args)
    {
        XRBaseInteractor interactor = args.interactor;

        if (interactor is XRDirectInteractor)
        {
            SavedTransform savedTransform = null;
            if (m_SavedTransforms.TryGetValue(interactor, out savedTransform))
            {
                interactor.attachTransform.localPosition = savedTransform.OriginalPosition;
                interactor.attachTransform.localRotation = savedTransform.OriginalRotation;

                m_SavedTransforms.Remove(interactor);
            }
        }


        /*var networkObject = args.interactable.GetComponent<NetworkObject>();

        if (_networkGrab != null)
            _networkGrab.RemoveOwnershipServerRPC(networkObject);*/

        base.OnSelectExiting(args);
    }

    public void ForceGrabEntering(SelectEnterEventArgs args)
    {
        base.OnSelectEntering(args);
    }

    public void ForceGrabExiting(SelectExitEventArgs args)
    {
        base.OnSelectExiting(args);
    }

    public override bool IsSelectableBy(XRBaseInteractor interactor)
    {
        int interactorLayerMask = 1 << interactor.gameObject.layer;
        return base.IsSelectableBy(interactor) && (interactionLayerMask.value & interactorLayerMask) != 0;
    }
}
