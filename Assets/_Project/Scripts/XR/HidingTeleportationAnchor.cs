using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidingTeleportationAnchor : MonoBehaviour
{

    [SerializeField] private Material _defaultMaterial;
    [SerializeField] private Material _selectedMaterial;
    [SerializeField] private TeleportationHand _teleportationHand;

    private MeshRenderer _meshRenderer;

    // Start is called before the first frame update
    void Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        bool visible = _teleportationHand.IsTeleporting();
        _meshRenderer.enabled = visible;

        if(visible)
        {
            var material = _teleportationHand.IsSelectedAnchor(this) ? _selectedMaterial : _defaultMaterial;
            _meshRenderer.material = material;
        }
    }
}
