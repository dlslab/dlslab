using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using Unity.Netcode;

[RequireComponent(typeof(XRSocketInteractor))]
public class NetworkSocket : NetworkBehaviour
{

    private XRSocketInteractor socket;
    private NetworkVariable<bool> hasObject = new NetworkVariable<bool>(false);
    private NetworkVariable<NetworkObjectReference> objInSocket = new NetworkVariable<NetworkObjectReference>();

    public GameObject InsertedObject { get
        {
            NetworkObjectReference objRef = objInSocket.Value;

            if(objRef.TryGet(out var networkObject))
            {
                return networkObject.gameObject;
            }

            return null;
        } 
    }

    public bool HasObject()
    {
        return hasObject.Value;
    }

    public bool HasObjectWithComponent(System.Type component)
    {
        var obj = InsertedObject;
        return hasObject.Value && obj != null && obj.GetComponent(component) != null;
    }

    // Start is called before the first frame update
    void Start()
    {
        socket = GetComponent<XRSocketInteractor>();

        if(HasObject())
        {
            ForceInsert(InsertedObject);
        }

        socket.selectEntered.AddListener(OnInsert);
        socket.selectExited.AddListener(OnRemove);
    }

    private void OnInsert(SelectEnterEventArgs args)
    {
        Debug.Log("Before forced");
        Debug.Log(args.GetType());
        if (args is ForcedSelectEnterEventArgs)
            return;

        Debug.Log("After forced");

        var networkObject = args.interactable.GetComponent<NetworkObject>();

        if(networkObject == null)
        {
            ForceRemove();
            return;
        }

        Debug.Log("Before server");
        ChangeInsertedObjectServerRPC(networkObject);
        Debug.Log("After server");
    }

    private void OnRemove(SelectExitEventArgs args)
    {
        if (args is ForcedSelectExitEventArgs)
            return;

        RemoveInsertedObjectServerRPC();
    }

    private void ForceInsert(GameObject obj)
    {
        Debug.Log("Force Insert");

        socket.selectEntered.Invoke(new ForcedSelectEnterEventArgs(socket, obj));
    }

    private void ForceRemove()
    {
        var interator = socket.GetComponent<XRBaseInteractor>();
        var interactable = socket.selectTarget;

        socket.selectExited.Invoke(new ForcedSelectExitEventArgs(socket));
    }

    /*// Update is called once per frame
    void Update()
    {
        ulong oldId = objInSocket.Value.NetworkObjectId;

        bool hasObject = socket.selectTarget != null;
        NetworkObject newObject = socket.selectTarget.GetComponent<NetworkObject>();
        ulong newId = hasObject ? newObject.NetworkObjectId : 0;

        if(this.hasObject.Value && !hasObject)
        {
            ChangeInsertedObjectServerRPC(null);
        }
        else if(!this.hasObject.Value && hasObject && newId != oldId)
        {
            ChangeInsertedObjectServerRPC(newObject);
        }
    }*/

    [ServerRpc(RequireOwnership = false)]
    private void ChangeInsertedObjectServerRPC(NetworkObjectReference obj)
    {
        hasObject.Value = true;
        objInSocket.Value = obj;

        UpdateInsertedObjectClientRPC(true, obj);
    }

    [ServerRpc(RequireOwnership = false)]
    private void RemoveInsertedObjectServerRPC()
    {
        hasObject.Value = false;
        objInSocket.Value = new NetworkObjectReference();
        UpdateInsertedObjectClientRPC(false, objInSocket.Value);
    }

    [ClientRpc]
    private void UpdateInsertedObjectClientRPC(bool hasObject, NetworkObjectReference objReference)
    {
        objReference.TryGet(out var gameObject);

        if (hasObject && gameObject != null)
        {
            ForceInsert(gameObject.gameObject);
        }
        else if(!hasObject)
            ForceRemove();
    }

    public class ForcedSelectEnterEventArgs : SelectEnterEventArgs
    {
        public ForcedSelectEnterEventArgs(XRSocketInteractor socket, GameObject gameObject)
        {
            interactor = socket;
            interactable = gameObject.GetComponent<XRBaseInteractable>();
        }
    }

    public class ForcedSelectExitEventArgs : SelectExitEventArgs
    {
        public ForcedSelectExitEventArgs(XRSocketInteractor socket)
        {
            interactor = socket;
            interactable = socket.selectTarget;
        }
    }
}
