using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class TeleportationHand : MonoBehaviour
{

    [SerializeField] private InputActionReference _teleportAction;
    [SerializeField] private XRRayInteractor _xrRayInterator;
    [SerializeField] private TeleportationProvider _teleportationProvider;
    private bool _isTeleporting = false;
    private HidingTeleportationAnchor _selectedAnchor;

    private bool IsClickingTeleport()
    {
        return _teleportAction.action.ReadValue<float>() == 1;
    }

    public bool IsTeleporting()
    {
        return _isTeleporting;
    }

    private void UpdateTeleportState()
    {
        if(!_isTeleporting && IsClickingTeleport())
        {
            _isTeleporting = true;
            OnTeleportStart();
        }
        else if(_isTeleporting && !IsClickingTeleport())
        {
            _isTeleporting = false;
            OnTeleportEnd();
        }
    }

    private void UpdateSelectedAnchor()
    {
        if(_isTeleporting)
        {
            bool found = _xrRayInterator.TryGetCurrent3DRaycastHit(out RaycastHit hit);

            if(found)
            {
                // Valid object if collider is HidingTeleportationAnchor, null if not.
                _selectedAnchor = hit.collider.GetComponent<HidingTeleportationAnchor>();
            }
        }
    }

    public bool IsSelectedAnchor(HidingTeleportationAnchor anchor)
    {
        return this._selectedAnchor == anchor;
    }

    private void TeleportToSelectedAnchor()
    {
        if(_selectedAnchor != null)
        {
            TeleportRequest req = new TeleportRequest()
            {
                destinationPosition = _selectedAnchor.gameObject.transform.position
            };

            _teleportationProvider.QueueTeleportRequest(req);
        }
    }

    // Called when the hand start the teleporting process
    private void OnTeleportStart()
    {
        _xrRayInterator.enabled = true;
    }

    // Called when the hand end the teleporting process
    private void OnTeleportEnd()
    {
        _xrRayInterator.enabled = false;
        TeleportToSelectedAnchor();
    }

    // Start is called before the first frame update
    void Start()
    {
        _xrRayInterator.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTeleportState();
        UpdateSelectedAnchor();
    }
}
