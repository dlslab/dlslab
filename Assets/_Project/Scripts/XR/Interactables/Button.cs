using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
///     Pressable button using hand collisions
/// </summary>
public class Button : XRBaseInteractable
{
    [SerializeField] protected UnityEvent pressEvent;
    [SerializeField] private float maxPress = 1.0f;
    [SerializeField] private float activatePress = 0.5f;
    private float initialY;
    private float minY;

    private Rigidbody rb;
    private XRBaseInteractor interactor;
    private float initialInteratorY;
    private bool pressed;
    
    protected override void Awake()
    {
        base.Awake();
        
        rb = GetComponent<Rigidbody>();
        
        hoverEntered.AddListener(OnPressStart);
        hoverExited.AddListener(OnPressEnd);
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        
        hoverEntered.RemoveListener(OnPressStart);
        hoverExited.RemoveListener(OnPressEnd);
    }

    /// <summary>
    ///     Called each tick when the user move the button
    /// </summary>
    /// <param name="updatePhase"></param>
    public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
    {
        if (interactor != null)
        {
            float newInteractorY = interactor.transform.position.y;
            float deltaY = newInteractorY - initialInteratorY;

            MoveButton(initialY + deltaY);
            VerifyClick();
        }
    }
    
    /// <summary>
    ///     Called when the user's hand enter the button collision
    /// </summary>
    private void OnPressStart(HoverEnterEventArgs args)
    {
        initialY = GetCurrentY();
        minY = initialY - maxPress;

        interactor = args.interactor;
        initialInteratorY = interactor.transform.position.y;
    }

    /// <summary>
    ///     Called when the user's hand moved away from the button collision
    /// </summary>
    private void OnPressEnd(HoverExitEventArgs args)
    {
        interactor = null;
        MoveButton(initialY);
    }

    /// <summary>
    ///     Moves the button to adapt press progress
    /// </summary>
    /// <param name="y">Local y position</param>
    private void MoveButton(float y)
    {
        Vector3 position = transform.position;
        position.y = Mathf.Clamp(y, minY, initialY);
        
        rb.MovePosition(position);
    }

    /// <summary>
    ///     Checks if the button is pressed enough to process the click
    /// </summary>
    private void VerifyClick()
    {
        if (initialY - GetCurrentY() > activatePress)
        {
            if(!pressed)
                pressEvent.Invoke();
            pressed = true;
        }
        else
            pressed = false;
    }

    private float GetCurrentY()
    {
        return transform.position.y;
    }

    public bool isPressed()
    {
        return pressed;
    }

}
