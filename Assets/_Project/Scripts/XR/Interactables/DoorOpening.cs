using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;
using Vector2 = UnityEngine.Vector2;
using Unity.Netcode;

public class DoorOpening : NetworkBehaviour
{
    [SerializeField] private Button openingButton;
    [SerializeField] private Transform hingePoint;
    [SerializeField] private float maxAngle = 90;
    [SerializeField] private Vector3 rotationAxis = Vector3.right;
    [SerializeField] private float acceleration = 1; 
    
    private Vector3 initialPosition;
    private Quaternion initialRotation;

    private NetworkVariable<bool> opening = new NetworkVariable<bool>();
    private NetworkVariable<bool> closing = new NetworkVariable<bool>();

    private NetworkVariable<float> currentSpeed = new NetworkVariable<float>(1);
    private NetworkVariable<float> currentAngle = new NetworkVariable<float>();

    public Vector3 RotationAxis => rotationAxis;

    private void Start()
    {
        initialPosition = transform.position;
        initialRotation = transform.rotation;
    }

    private void Update()
    {
        if (!IsServer && !IsHost)
            return;

        if (opening.Value)
        {
            if (!isCompletelyOpened())
            {
                currentSpeed.Value += acceleration;
                MoveDoor(currentSpeed.Value * Time.deltaTime);
            }
            else
            {
                opening.Value = false;
            }
        }

        if(closing.Value)
        {
            if(!isCompletelyClosed())
            {
                currentSpeed.Value += acceleration;
                MoveDoor(-currentSpeed.Value * Time.deltaTime);
            }
            else
            {
                closing.Value = false;
                CompletelyCloseDoor();
            }
        }
    }

    private void MoveDoor(float angle)
    {
        float angleAddition = Mathf.Clamp(angle, -currentAngle.Value, maxAngle - currentAngle.Value);
        currentAngle.Value += angleAddition;
        transform.RotateAround(hingePoint.position, rotationAxis, angleAddition);
    }
    
    public bool isCompletelyOpened()
    {
        return currentAngle.Value >= maxAngle;
    }

    public bool isCompletelyClosed()
    {
        return currentAngle.Value < 10;
    }

    [ServerRpc(RequireOwnership = false)]
    public void OpenDoorServerRPC()
    {
        closing.Value = false;
        opening.Value = true;
        currentSpeed.Value = 1;
    }

    [ServerRpc(RequireOwnership = false)]
    public void CloseDoorServerRPC()
    {
        opening.Value = false;
        closing.Value = true;
        currentSpeed.Value = 1;
    }

    public void CompletelyCloseDoor()
    {
        transform.position = initialPosition;
        transform.rotation = initialRotation;
    }

    public bool CanClose()
    {
        return !closing.Value && !opening.Value && !isCompletelyClosed();
    }
    
}
