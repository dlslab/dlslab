using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
///     Class handling collisions for <see cref="DoorOpening"/>
/// </summary>
public class DoorClosingInteractable : XRBaseInteractable
{
    private XRBaseInteractor interactor;
    private Vector3 initialInteratorPosition;
    private DoorOpening doorOpening;

    protected override void Awake()
    {
        Debug.Log("Door interact awake");
        base.Awake();

        doorOpening = GetComponent<DoorOpening>();

        hoverEntered.AddListener(OnHandlingStart);
        hoverExited.AddListener(OnHandlingEnd);
    }

    protected override void OnDestroy()
    {
        hoverEntered.RemoveListener(OnHandlingStart);
        hoverExited.RemoveListener(OnHandlingEnd);
    }

    private void OnHandlingStart(HoverEnterEventArgs args)
    {
        Debug.Log("Door interact handle start");
        if (doorOpening.CanClose())
        {
            interactor = args.interactor;
            initialInteratorPosition = interactor.transform.position;
        }
    }

    private void OnHandlingEnd(HoverExitEventArgs args)
    {
        interactor = null;
    }

    public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
    {
        if (interactor != null && doorOpening.CanClose())
        {
            Vector3 currentInteractorPosition = interactor.transform.position;
            Vector2 movement = ignoreAxis(initialInteratorPosition - currentInteractorPosition, doorOpening.RotationAxis);

            bool makeDoorClose = movement.x > 0 || movement.y < 0;

            if (makeDoorClose)
            {
                doorOpening.CloseDoorServerRPC();
            }

        }
    }

    /// <summary>
    ///     Converts a Vector3 to Vector2 removing the specified axis. Note : x, y, z might not be the same variable in the Vector2. Example : Y in Vector3 will become X if X is ignored
    /// </summary>
    /// <param name="vector3">Vector to convert</param>
    /// <param name="ignoredAxis">Axis to remove</param>
    /// <returns>Vector2 without the specified axis</returns>
    private Vector2 ignoreAxis(Vector3 vector3, Vector3 ignoredAxis)
    {
        float[] arrVector3 = new float[] { vector3.x, vector3.y, vector3.z };
        float[] arrVector2 = new float[2];

        int v2Index = 0;
        for (int i = 0; i < 3; i++)
        {
            if (ignoredAxis[i] == 0)
            {
                arrVector2[v2Index] = arrVector3[i];
            }
        }

        return new Vector2(arrVector2[0], arrVector2[1]);
    }
}
