﻿using UnityEngine;
using UnityEngine.Video;
using System.Collections;
using System.Collections.Generic;

/// <summary>
///     Used to find which video to play to represent a DLS analysis
/// </summary>
public class ClipSelector : MonoBehaviour
{
    /// <summary>
    ///     Parameters and video set in the editor
    /// </summary>
    [SerializeField] private ClipEntry[] entries;


    /// <summary>
    ///     Videos in <see cref="entries"/> but in a Dictionnary to easier access
    /// </summary>
    private Dictionary<ClipId, VideoClip> clips = new Dictionary<ClipId, VideoClip>();

    private void Start()
    {
        LoadVideos();
    }

    /// <summary>
    ///     Loads videos from the ClipEntry array to the <ClidId, VideoClip> dictionnary
    /// </summary>
    private void LoadVideos()
    {
        foreach (ClipEntry entry in entries)
        {
            clips.Add(entry.Identifier, entry.Clip);
        }
    }

    /// <summary>
    ///     Find the <see cref="VideoClip">VideoClip</see> from <see cref="clips"/> with the specified parameters
    /// </summary>
    /// <param name="analysisType">Type of analysis</param>
    /// <param name="liquid">Liquid used in the analysis</param>
    /// <returns>The corresponding clip if found or defaults to the clip with SpecialType NOT_FOUND</returns>
    public VideoClip GetClip(AnalysisType analysisType, Liquid liquid)
    {
        bool found = clips.TryGetValue(new ClipId(analysisType, liquid), out var clip);

        if (!found)
            return GetClip(analysisType, SpecialType.NOT_FOUND);

        return clip;
    }

    /// <summary>
    ///      Find the <see cref="VideoClip">VideoClip</see> from <see cref="clips"/> with the specified parameters
    /// </summary>
    /// <param name="analysisType">Type of analysis</param>
    /// <param name="specialType">Type of particuliar analysis, same video for every liquids</param>
    /// <returns></returns>
    /// <exception cref="KeyNotFoundException">Throws exception if clip not found with specified parameters</exception>
    public VideoClip GetClip(AnalysisType analysisType, SpecialType specialType)
    {
        bool found = clips.TryGetValue(new ClipId(analysisType, specialType), out var clip);

        if (!found)
            throw new KeyNotFoundException("Clip could not be found for this type and special ID (" + specialType.ToString() + ")");

        return clip;
    }
}

/// <summary>
///     Type of DLS Analysis
/// </summary>
public enum AnalysisType
{
    PSD = 1,
    ZETA = 2
}


/// <summary>
///     Type of videos for special cases. Same VideoClip for all liquids.
/// </summary>
public enum SpecialType
{
    NONE = 0,
    EMPTY = 1,
    NOT_FOUND = 2
}

/// <summary>
///     Represent a video and the parameters where the video plays.
/// </summary>
[System.Serializable]
public struct ClipEntry
{
    /// <summary>
    ///     Conditions where we play the video
    /// </summary>
    [SerializeField] private ClipId _identifier;

    /// <summary>
    ///     VideoClip to play
    /// </summary>
    [SerializeField] private VideoClip _clip;

    /// <summary>
    ///     Conditions where we play the video
    /// </summary>
    public ClipId Identifier => _identifier;

    /// <summary>
    ///     VideoClip to play
    /// </summary>
    public VideoClip Clip => _clip;
}

/// <summary>
///     Represents the conditions needed to play a video clip.
/// </summary>
[System.Serializable]
public struct ClipId
{
    [SerializeField] private AnalysisType _analysisType;
    [SerializeField] private Liquid _liquid;
    [SerializeField] private SpecialType _specialType;

    public override int GetHashCode()
    {
        if (_specialType > 0)
        {
            return (((int)_analysisType) + ";S;" + _specialType.GetHashCode()).GetHashCode();
        }
            

        return (((int)_analysisType) + ";N;" + _liquid.GetHashCode()).GetHashCode();
    }

    public override bool Equals(object obj)
    {
        return GetHashCode() == obj.GetHashCode();
    }

    public ClipId(AnalysisType type, Liquid liquid)
    {
        _analysisType = type;
        _liquid = liquid;
        _specialType = 0;
    }

    public ClipId(AnalysisType type, SpecialType specialType)
    {
        _analysisType = type;
        _liquid = null;
        _specialType = specialType;
    }
}