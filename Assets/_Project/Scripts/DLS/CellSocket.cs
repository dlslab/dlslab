using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

/// <summary>
///     Special socket where you can only place DLS cell
/// </summary>
public class CellSocket : XRSocketInteractor
{
    [SerializeField] private DoorOpening door;

    public override bool CanSelect(IXRSelectInteractable interactable)
    {
        /*if (door.isCompletelyClosed())
            return false;*/

        if (interactable.transform.gameObject.GetComponent<Cell>() == null)
            return false;

        return base.CanSelect(interactable);
    }
}
