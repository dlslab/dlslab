using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     Identity a GameObject as a DLS cell
/// </summary>
public class Cell : MonoBehaviour
{
    [SerializeField] private GameObject _liquid;

    /// <summary>
    ///     Liquid present in the the cell
    /// </summary>
    public Liquid Liquid => _liquid.GetComponent<Liquid>();
}
