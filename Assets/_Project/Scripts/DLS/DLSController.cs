using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.XR.Interaction.Toolkit;
using Unity.Netcode;
using Material = UnityEngine.Material;

/// <summary>
///     Controls the DLS
/// </summary>
public class DLSController : NetworkBehaviour
{
    [SerializeField] private NetworkSocket cuveSocket;
    [SerializeField] private DoorOpening door;
    [SerializeField] private GameObject ledObject;

    [SerializeField] private Material invalidLedMaterial;
    [SerializeField] private Material validLedMaterial;

    [SerializeField] private VideoPlayer videoPlayer;
    private ClipSelector clipSelector;

    private bool wasValid = false;
    private NetworkVariable<bool> isValid = new NetworkVariable<bool>(false);
    private NetworkVariable<double> currentVideoTime = new NetworkVariable<double>(0.0);

    private void Start()
    {
        clipSelector = GetComponent<ClipSelector>();

        OnValidChange(isValid.Value);
    }

    private void Update()
    {
        if(IsServer || IsHost)
        {
            bool isValid = IsValid();
            if (wasValid != isValid)
            {
                wasValid = isValid;
                this.isValid.Value = isValid;
                OnValidChangeClientRpc(isValid);
            }

            if (isValid)
            {
                currentVideoTime.Value = videoPlayer.time;
            }
        }
    }

    public Cell GetCuve()
    {
        GameObject gameObject = cuveSocket.InsertedObject;

        if (gameObject != null)
            return gameObject.GetComponent<Cell>();

        return null;
    }

    public bool HasCuve()
    {
        return cuveSocket.HasObjectWithComponent(typeof(Cell));
    }

    public bool IsValid()
    {
        bool isClosed = door.isCompletelyClosed();
        bool hasCuve = HasCuve();

        //Debug.Log(isClosed + " " + hasCuve);

        return isClosed && hasCuve;
    }

    private VideoClip FindVideo()
    {
        Cell cuve = GetCuve();
        return clipSelector.GetClip(AnalysisType.PSD, cuve.Liquid);
    }

    public void StartVideo()
    {
        if(HasCuve())
        {
            videoPlayer.transform.parent.gameObject.SetActive(true);
            videoPlayer.clip = FindVideo();
            videoPlayer.Play();
        }

    }

    public void StopVideo()
    {
        videoPlayer.Stop();
        videoPlayer.transform.parent.gameObject.SetActive(false);
    }

    [ClientRpc]
    public void OnValidChangeClientRpc(bool valid)
    {
        OnValidChange(valid);
    }

    public void OnValidChange(bool valid)
    {
        var mesh = ledObject.GetComponent<MeshRenderer>();
        mesh.material = valid ? validLedMaterial : invalidLedMaterial;

        if (valid)
        {
            StartVideo();
        }
        else
            StopVideo();
    }

    public void OpenDoor()
    {
        door.OpenDoorServerRPC();
    }

}
