using Unity.Netcode;
using UnityEngine;

/// <summary>
///     Updates the object Transform to match the owner's object transform. 
///     Everyone will see the object at the same place as the object's owner
/// </summary>
public class OwnerAuthoritativeTransform : NetworkBehaviour
{
    private NetworkVariable<Vector3> position = new NetworkVariable<Vector3>();
    private NetworkVariable<Quaternion> rotation = new NetworkVariable<Quaternion>();

    /// <summary>
    ///     Set intial position when the server starts.
    ///     Called both at Start and OnServerStarted to adapts it to Host and Server mode.
    /// </summary>
    private void SetIntialPosition()
    {
        if (IsServer || IsHost)
        {
            position.Value = transform.position;
            rotation.Value = transform.rotation;

            DebugUtils.Log("Initial : " + position.Value.ToString() + " " + rotation.Value.ToString());
        }
    }

    /// <summary>
    ///     Updates the object location with the server data
    /// </summary>
    private void UpdateLocationFromServer()
    {
        transform.position = position.Value;
        transform.rotation = rotation.Value;
    }

    void Start()
    {
        SetIntialPosition();
        NetworkManager.Singleton.OnServerStarted += SetIntialPosition;
    }

    void Update()
    {
        //Stops if not connected
        if (NetworkManager.Singleton.LocalClient == null)
            return;

        if((IsClient || IsHost) && IsOwner)
        {
            UpdateLocationServerRPC(transform.position, transform.rotation);
        }
        else
        {
            UpdateLocationFromServer();
        }
    }

    /// <summary>
    ///     Sends a RPC to the server to update the object location
    /// </summary>
    [ServerRpc]
    public void UpdateLocationServerRPC(Vector3 position, Quaternion rotation)
    {
        this.position.Value = position;
        this.rotation.Value = rotation;
    }


    /// <summary>
    ///     Grant the ownership to the connected user, making his object transform the "real" one.
    ///     Calls a ServerRPC <see cref="GetOwnershipServerRPC(ulong, NetworkObjectReference)"/>.
    /// </summary>
    public void GetOwnership()
    {
        ulong clientId = NetworkManager.Singleton.LocalClientId;
        GetOwnershipServerRPC(clientId, this.NetworkObject);
    }

    /// <summary>
    ///     Removes the ownwership of the object, lets the server decide the location.
    ///     Calls a ServerRPC <see cref="RemoveOwnershipServerRPC(NetworkObjectReference)"/>
    /// </summary>
    /// 
    public void RemoveOwnership()
    {
        RemoveOwnershipServerRPC(this.NetworkObject);
    }

    /// <summary>
    ///     Update the ownership on the server-side
    /// </summary>
    /// <param name="clientId">NetworkManager LocalClientId representing the new owner</param>
    /// <param name="obj">Object to update the ownership</param>
    [ServerRpc(RequireOwnership = false)]
    private void GetOwnershipServerRPC(ulong clientId, NetworkObjectReference obj)
    {
        if (obj.TryGet(out var networkObject))
        {
            networkObject.ChangeOwnership(clientId);
        }
        else
            Debug.LogError("Couldn't get network object to take ownership");
    }

    /// <summary>
    ///     Remove the ownership on the server-side
    /// </summary>
    /// <param name="obj">Object to remove the ownership</param>
    [ServerRpc(RequireOwnership = true)]
    private void RemoveOwnershipServerRPC(NetworkObjectReference obj)
    {
        if (obj.TryGet(out var networkObject))
        {
            networkObject.RemoveOwnership();
        }
        else
            Debug.LogError("Couldn't get network object to remove ownership");
    }

}
