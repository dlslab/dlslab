using System;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.Netcode.Transports.UNET;
using UnityEngine;

/// <summary>
///     Utility that conntext the network manager with the good settings
/// </summary>
public class NetworkCommandLine : MonoBehaviour
{
    private NetworkManager netManager;
    private UNetTransport transport;

    void Start()
    {
        DebugUtils.Log("App started.");
        netManager = GetComponentInParent<NetworkManager>();
        transport = netManager.GetComponent<UNetTransport>();

        if (Application.isEditor) return;

        var args = GetCommandlineArgs();

        if(args.TryGetValue("-username", out string name))
        {
            UserInfos.Instance.UserName = name;
        }

        if (args.TryGetValue("-serverhost", out string host))
        {
            transport.ConnectAddress = host;
        }

        if (args.TryGetValue("-serverport", out string strPort))
        {
            int port = int.Parse(strPort.Trim());
            transport.ConnectPort = port;
            transport.ServerListenPort = port;
        }

        RegisterServerEvents();

        if (args.TryGetValue("-mlapi", out string mlapiValue))
        {
            switch (mlapiValue)
            {
                case "server":
                    netManager.StartServer();
                    break;
                case "host":
                    netManager.StartHost();
                    break;
                case "client":
                    DebugUtils.Log(String.Format("Connecting to {0}:{1} ...", transport.ConnectAddress, transport.ConnectPort));
                    netManager.StartClient();
                    break;
            }
        }
        else
        {
            transport.ServerListenPort = 7777;
            netManager.StartHost();
        }

    }

    private void RegisterServerEvents()
    {
        netManager.OnClientConnectedCallback += OnClientConnectOnServer;
        netManager.OnClientDisconnectCallback += OnClientDiscornectOnServer;
        netManager.OnServerStarted += OnServerStart;
    }

    private void OnClientConnectOnServer(ulong uid)
    {
        DebugUtils.Log("Client connected " + uid);
    }

    private void OnClientDiscornectOnServer(ulong uid)
    {
        DebugUtils.Log("Client disconnected " + uid);
    }

    private void OnServerStart()
    {
        DebugUtils.Log(String.Format("Server started on {0}:{1}", transport.ConnectAddress, transport.ConnectPort));
    }

    /// <summary>
    ///     Place the command args in a useful Dictionnary
    /// </summary>
    /// <returns>
    ///     Dictionary argName, value
    /// </returns>
    private Dictionary<string, string> GetCommandlineArgs()
    {
        Dictionary<string, string> argDictionary = new Dictionary<string, string>();

        var args = System.Environment.GetCommandLineArgs();

        for (int i = 0; i < args.Length; ++i)
        {
            var arg = args[i].ToLower();
            if (arg.StartsWith("-"))
            {
                var value = i < args.Length - 1 ? args[i + 1] : null;
                value = (value?.StartsWith("-") ?? false) ? null : value;

                argDictionary.Add(arg, value);
            }
        }
        return argDictionary;
    }

}
