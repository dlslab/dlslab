﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

static class DebugUtils
{

    /// <summary>
    ///     Logs with more useful format for debugging
    /// </summary>
    /// <param name="msg">Message to log</param>
    public static void Log(string msg)
    {
        Debug.Log(String.Format("[{0}] {1}", DateTime.Now.ToString(), msg));
    }
}