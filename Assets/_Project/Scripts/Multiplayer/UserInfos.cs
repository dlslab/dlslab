using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using Unity.Netcode.Components;
using Unity.Collections;

/// <summary>
///     Singleton storing the connected user infos
/// </summary>
public class UserInfos
{

    public string UserName { get; set; } = "User";


    private static UserInfos _instance;
    public static UserInfos Instance
    {
        get
        {
            if (_instance == null)
                _instance = new UserInfos();

            return _instance;
        }
    }

}
