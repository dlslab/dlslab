using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class TestInput : MonoBehaviour
{

    public void Show()
    {
        var devices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Right, devices);

        Debug.Log(devices.Count + " devices");
        foreach (var d in devices)
        {
            Debug.Log(d.name);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Show();
    }

    int updates = 0;

    // Update is called once per frame
    void Update()
    {
        if(updates <= 100)
        {
            updates++;
        }
        else
        {
            updates = 0;
            Show();
        }
    }
}
