@echo off

set type=host
set host=192.168.2.250
set port=7777
set file=log-%type%.txt
set monitor=0

START /B .\LabTraining.exe -mlapi %type% -serverhost %host% -port %port% -logfile %file%

:readLog

IF %monitor% EQU 1 (
	cls
	type %file%
	TIMEOUT 1 /NOBREAK

	goto readLog
)
